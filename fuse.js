//@ts-check
const {
   CSSPlugin,
   FuseBox,
   Sparky,
   WebIndexPlugin,
   QuantumPlugin,
   EnvPlugin,
   CopyPlugin,

} = require("fuse-box");
const process = require("process");
const isProd =
   process.env.NODE_ENV != undefined ? !process.env.NODE_ENV.includes("dev") : false;
let fuse;

const fuseOptions = {
   homeDir: "./src",
   output: "dist/$name.js",
   // sourceMaps: !isProd,
   //useTypescriptCompilier: true
};
const fuseClientOptions = {
   ...fuseOptions,
   plugins: [
      CopyPlugin({
         files: ["*.txt", "*.png"],
         dest: "dist",
         resolve: "/assets/img/"
      }),
      CSSPlugin({
         inject: true
      }),

      isProd &&
         WebIndexPlugin({
            template: "./src/client/index.html"
         }),
      isProd &&
         QuantumPlugin({
            bakeApiIntoBundle: "bundle",
            treeshake: true,
            uglify: true,
            css: true
         }),
      isProd &&
         EnvPlugin({
            NODE_ENV: "production"
         })
   ]
};
const fuseServerOptions = {
   ...fuseOptions,
   plugins: [CSSPlugin()]
};
Sparky.task("clean", async () => {
   await Sparky.src("./dist")
      .clean("dist/")
      .exec();
});
Sparky.task("config", () => {
   fuse = FuseBox.init(fuseOptions);
   fuse.dev();
});
Sparky.task("test", ["&clean", "&config"], () => {
   fuse.bundle("client/bundle").test("[**/**.test.tsx]", null);
});
Sparky.task("standalone", ["&clean"], () => {
   fuse = FuseBox.init(fuseClientOptions);
   fuse
      .bundle("bundle")
      .target("browser@esnext")
      .instructions("> client/standalone.tsx");
   fuse.run();
   // fuse.dev();
});
Sparky.task("client", () => {
   fuse.opts = fuseClientOptions;
   fuse
      .bundle("client/bundle")
      .target("browser@esnext")
      .watch("client/**")
      .hmr()
      .instructions("> client/index.tsx");
});
Sparky.task("server", () => {
   /**Workaround. Should be fixed */
   fuse.opts = fuseServerOptions;
   fuse
      .bundle("server/bundle")
      // server should restart even if client was changed
      .watch("**")
      .instructions("> [server/index.tsx] + fuse-box-css")
      .completed(proc => {
         proc.require({
            // tslint:disable-next-line:no-shadowed-variable
            close: ({ FuseBox }) => FuseBox.import(FuseBox.mainFile).shutdown()
         });
      });
});
Sparky.task("dev", ["&clean", "&config", "&server", "&client"], () => {
   fuse.run();
});
