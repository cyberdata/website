import "bootstrap/dist/css/bootstrap.min.css";
import * as React from "react";
import { render } from "react-dom";
import { HashRouter } from "react-router-dom";
// import "../public/css/Header-Blue.css";
// import "../public/css/Highlight-Blue.css";
// import "../public/css/Highlight-Phone.css";
// import "../public/css/Navigation-Clean.css";
// import "../public/css/Map-Clean.css";
// import "../public/css/Projects-Clean.css";
// import "../public/css/Team-Clean.css";
// import "../public/css/Footer-Basic.css";
// import "../public/css/priem_rtf.css";

// import "../public/fonts/font-awesome.min.css";
// import "../public/fonts/ionicons.min.css";
// import "../public/fonts/material-icons.min.css";
// import "../public/fonts/simple-line-icons.min.css";
// import "../public/fonts/typicons.min.css";
import App from "./components/App/App";
const wrapper = (
   <HashRouter>
      <App />
   </HashRouter>
);
render(wrapper, document.getElementById("root"));
