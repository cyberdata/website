import * as React from "react";
import { Route, Switch } from "react-router";
import { Link } from "react-router-dom";
import About from "../About/About";
import Home from "../Home/Home";
interface IState {}
interface IProps {}
export default class App extends React.Component<IProps, IState> {
   constructor(props: IProps) {
      super(props);
   }
   public render() {
      return (
         <div>
            {/* {this.} */}
            {/* <Link to="/Home">
               <p>Home</p>
            </Link>
            <Link to="/About" className="link">
               <p>About</p>
            </Link> */}
            {/* Main page */}
            <Switch>
               <Route exact path="/Home" component={Home} />
               <Route exact path="/" component={About} />
            </Switch>
         </div>
      );
   }
}
