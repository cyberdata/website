import * as React from "react";
interface IState {}
interface IProps {}
export default class Home extends React.Component<IProps, IState> {
   constructor(props: IProps) {
      super(props);
   }
   protected click() {
      /**
       * Try to debug next line
       */
      console.log("hi");
   }
   public render() {
      return (
         <div >
            <p>Home page</p>
            <button onClick={this.click}>Click me</button>
         </div>
      );
   }
}
