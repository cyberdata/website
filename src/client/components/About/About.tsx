import * as React from "react";
interface IState {
   clickCount: number;
   showMore: boolean;
}
interface IProps {
   history?: any;
}
export default class About extends React.Component<IProps, IState> {
   constructor(props: IProps) {
      super(props);
      this.state = {
         clickCount: 0,
         showMore: false
      };
      this.increment = this.increment.bind(this);
      this.showMoreClick = this.showMoreClick.bind(this);
   }
   protected increment() {
      this.setState({
         clickCount: this.state.clickCount + 1
      });
   }
   protected showMoreClick() {
      this.setState({
         showMore: !this.state.showMore
      });
      console.log("show modal");
   }
   private ShowMoreModal() {
      return (
         <div
            className="modal fade show"
            tabIndex={-1}
            id="showMoreModal"
            style={{
               display: "block"
            }}
         >
            <div className="modal-dialog modal-dialog-centered" role="dialog">
               <div className="modal-content">
                  <div className="modal-body">
                     <p>
                        В настоящее время институт оснащен самым лучшим оборудованием ведущих
                        мировых фирм, которое позволяет проводить исследования в области теории
                        электромагнитного поля, электромагнитной совместимости, радиоэлектроники,
                        телекоммуникаций, информационных технологий и систем управления на самом
                        высоком уровне. в институте работает замечательный творческий коллектив,
                        сочетающий в себе опыт профессоров старшего поколения с творческой энергией
                        и инициативностью молодежи.За 66 лет выпущено более 25000 специалистов, что
                        составляет десятую часть выпускников.{" "}
                     </p>
                  </div>
                  <div className="modal-footer">
                     <button type="button" className="btn btn-primary" onClick={this.showMoreClick}>
                        Понятно
                     </button>
                  </div>
               </div>
            </div>
         </div>
      );
   }
   public render() {
      return (
         <div>
            {this.state.showMore ? this.ShowMoreModal() : null}
            <div
               style={{
                  opacity: this.state.showMore ? 0.6 : 1
               }}
            >
               <nav
                  className="navbar navbar-light navbar-expand-md navigation-clean"
                  style={{ backgroundColor: "rgba(255,255,255,0.8)", paddingTop: 10 }}
               >
                  <div className="container">
                     <a className="navbar-brand text-dark" style={{ fontSize: 24 }}>
                        <i className="fa fa-wifi" />
                        &nbsp;Радиотехника
                     </a>
                     <button
                        className="navbar-toggler"
                        data-toggle="collapse"
                        data-target="#navcol-1"
                     >
                        <span className="sr-only">Toggle navigation</span>
                        <span className="navbar-toggler-icon" />
                     </button>
                     <div className="collapse navbar-collapse" id="navcol-1">
                        <ul className="nav navbar-nav ml-auto">
                           {/* <li className="nav-item" role="presentation">
                           <a className="nav-link">Вход</a>
                        </li>
                        <li className="nav-item" role="presentation">
                           <a className="nav-link">Регистрация</a>
                        </li>
                        <li className="dropdown">
                           <a
                              className="dropdown-toggle nav-link dropdown-toggle"
                              data-toggle="dropdown"
                              aria-expanded="false"
                           >
                              О нас&nbsp;
                           </a>
                           <div className="dropdown-menu" role="menu">
                              <a className="dropdown-item" role="presentation">
                                 First Item
                              </a>
                              <a className="dropdown-item" role="presentation">
                                 Second Item
                              </a>
                              <a className="dropdown-item" role="presentation">
                                 Third Item
                              </a>
                           </div>
                        </li> */}
                        </ul>
                     </div>
                  </div>
               </nav>
               <div>
                  <div className="header-blue acrylic medium">
                     <div className="container hero">
                        <div className="row d-flex">
                           <div className="col-12 col-lg-6 col-xl-5 offset-xl-1">
                              <h1>
                                 Радиотехника — это круто.
                                 <br />
                              </h1>
                              <p>
                                 Радиотехника — это область науки и техники, изучающая
                                 электромагнитные колебания и волны радиодиапазона, методы
                                 генерации, усиления, преобразования, излучения и приёма, а также
                                 применение их для передачи информации.
                                 <br />
                              </p>
                              <button
                                 onClick={this.showMoreClick}
                                 data-target="#exampleModal"
                                 className="btn btn-light btn-lg action-button"
                                 type="button"
                              >
                                 Узнать больше
                              </button>
                           </div>
                           <div className="col-auto col-lg-6 col-xl-5 offset-0 offset-xl-1 d-flex flex-row justify-content-between align-items-stretch align-content-stretch align-self-center m-auto">
                              <img
                                 className="d-flex flex-column flex-grow-1 flex-shrink-1 flex-fill align-self-center"
                                 src="./assets/img/sputnik.png"
                                 width={250}
                              />
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div className="map-clean">
                  <div className="container">
                     <div className="intro">
                        <h2 className="text-center">РТФ, УРФУ, ПОПОВ!</h2>
                        <p className="text-center">ИРИТ-РТФ</p>
                     </div>
                  </div>
                  <iframe
                     allowFullScreen
                     frameBorder={0}
                     width="100%"
                     height={350}
                     src="https://www.google.com/maps/embed/v1/streetview?key=AIzaSyDCrk6YkRDqU9yjiASQH4IJGKVFAMOR4UI&location=56.8408117%2C60.6512818"
                  />
               </div>
               <div className="common">
                  <div className="item form">
                     <div className="title">Форма обучения</div>
                     <div className="text">
                        Очная <br />
                        Очно-заочная <br />
                        Заочная <br />
                        <div className="title">(бюджет, контракт)</div>
                     </div>
                  </div>
                  <div className="item seats">
                     <div className="title">Бюджетных мест</div>
                     <div className="text big">40</div>
                  </div>
                  <div className="item ege">
                     <div className="title">Вступительные испытания ЕГЭ</div>
                     <div className="text small">
                        Математика, 55 <br />
                        Информатика и ИКТ, 55
                        <br />
                        Русский язык, 36
                     </div>
                  </div>
                  <div className="item time">
                     <div className="title">Срок обучения</div>
                     <div className="text">
                        4<br />
                        3,5
                        <br />
                        3,5
                     </div>
                  </div>
                  <div className="item qualification">
                     <div className="title">Квалификация</div>
                     <div className="text big">Бакалавр</div>
                  </div>
               </div>
               {/* <p className="text-monospace lead text-center" style={{ fontSize: 45 }}>
               Средний балл ЕГЭ по нашему направлению
               <br />
               208 баллов
               <br />
            </p> */}
               <div className="article-clean">
                  <div className="container">
                     <div className="row">
                        <div className="col-lg-10 col-xl-8 offset-lg-1 offset-xl-2">
                           <div className="intro">
                              <h1 className="text-center">
                                 Почему стоит поступать на радиотехнику?
                              </h1>
                           </div>
                           <div className="text">
                              <h2>Образовательная программа</h2>
                              <p>
                                 Образовательная программа направлена на подготовку специалистов,
                                 которые смогут создавать и обеспечивать функционирование устройств
                                 и систем, основанных на использовании электромагнитных колебаний и
                                 волн. Эти приборы и механизмы предназначены для передачи, приема и
                                 обработки информации, получения информации об окружающей среде,
                                 природных и технических объектах, а также для воздействия на
                                 природные или технические объекты с целью изменения их свойств.
                                 <br />
                              </p>
                              <h2>
                                 Изучаемые дисциплины
                                 <br />
                              </h2>
                              <p>
                                 В рамках программы «Радиотехника» изучаются дисциплины
                                 математического и естественно-научного, гуманитарного,
                                 экономического и профессионального циклов. Профессиональную
                                 деятельность выпускник сможет выполнять в организациях и
                                 предприятиях по профилям приборостроения, связи и
                                 телекоммуникации.В настоящее время специалисты окончившие
                                 Радиотехническое направление нужны в разных рабочих сферах. Так
                                 например, специалисты с такими знаниями необходимы в
                                 научно-исследовательских институтах, на заводах по производству
                                 техники, в медицинских учреждениях и различных фирмах сотовой
                                 связи. <br />
                                 Основные дисциплины:{" "}
                                 <ul>
                                    <li>Высшая математика</li>
                                    <li>Физика </li>
                                    <li> Информатика </li>
                                    <li> Электроника </li>
                                    <li>Основы теории цепей </li>
                                    <li>Радиотехнические цепи и сигналы </li>
                                    <li>Устройства СВЧ и антенны</li>
                                    <li>Основы формирования и обработки сигналов</li>
                                 </ul>
                                 <br />
                              </p>
                              <h2>
                                 Программа обучения
                                 <br />
                              </h2>
                              <p>
                                 Подготовка бакалавра в области исследований и разработки,
                                 направленных на создание и обеспечение функционирования устройств и
                                 систем, основанных на использовании электромагнитных колебаний и
                                 волн и предназначенных для передачи, приема и обработки информации,
                                 получения информации об окружающей среде, природных и технических
                                 объектах, а также для воздействия на природные или технические
                                 объекты с целью изменения их свойств. Программа обеспечивает
                                 глубокие теоретические знания и практические навыки в области
                                 построения и функционирования цифровых и аналоговых
                                 радиоэлектронных устройств в составе телекоммуникационных,
                                 радиолокационных и радионавигационных систем, современным
                                 программным обеспечением для их моделирования и разработки,
                                 знакомство с современными достижениями в области техники и
                                 технологии высоких и сверхвысоких частот.В настоящее время имеется
                                 острая необходимость в кадрах высокой квалификации, обладающих
                                 знаниями в области теории и практики современных систем
                                 радиотехники, радиолокации, навигации, передачи данных,
                                 проектирования радиоэлектронных и телекоммуникационных устройств,
                                 способных на научной основе совершенствовать существующие и
                                 разрабатывать перспективное устройства и системы.Объектами
                                 профессиональной деятельности выпускников являются радиотехнические
                                 системы, комплексы и устройства, методы и средства их
                                 проектирования, моделирования, экспериментальной отработки,
                                 подготовки к производству и технического обслуживания.
                                 <br />
                              </p>
                              <figure />
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div className="article-list">
                  <div className="container">
                     <div className="intro">
                        <h2 className="text-center">Профессии</h2>
                        <p className="text-center">
                           Все профессии важны, все профессии нужны!
                           <br />
                        </p>
                     </div>
                     <div className="row articles">
                        <div className="col-sm-6 col-md-4 item">
                           <a href="#">
                              <img className="img-fluid" src="./assets/img/radioeng.PNG" />
                           </a>
                           <h3 className="name">
                              Инженер-радиоэлектронщик
                              <br />
                           </h3>
                           <p className="description">
                              Инженер-радиоэлектронщик занимается разработкой, проектированием,
                              исследованием и эксплуатацией радиоэлектронных средств и
                              радиоэлектронных систем различного назначения. Также такой специалист
                              создает и совершенствует методы и средства преобразования информации,
                              обмена информации на расстоянии с помощью радиоэлектронных средств и
                              технологий, обеспечивающих передачу, излучение и прием передаваемой
                              информации по сетям радиосвязи различного назначении.
                              <br />
                           </p>
                        </div>
                        <div className="col-sm-6 col-md-4 item">
                           <a href="#">
                              <img className="img-fluid" src="./assets/img/radioengit.PNG" />
                           </a>
                           <h3 className="name">
                              Специалист по радиосвязи и телекоммуникациям
                              <br />
                           </h3>
                           <p className="description">
                              Специалист по радиосвязи и телекоммуникациям занимается эксплуатацией
                              и развитием систем радиосвязи и телекоммуникационных систем. Его
                              главная задача - обеспечение эксплуатации и развития систем радиосвязи
                              и телекоммуникационных систем, включая коммутационные подсистемы и
                              сетевые платформы, сети радиодоступа, транспортные сети и сети
                              передачи данных, спутниковые системы связи.
                              <br />
                           </p>
                        </div>
                        <div className="col-sm-6 col-md-4 item">
                           <a href="#">
                              <img className="img-fluid" src="./assets/img/radioengspace.PNG" />
                           </a>
                           <h3 className="name">
                              Радиоинженер в ракетно-космической промышленности
                              <br />
                           </h3>
                           <p className="description">
                              Радиоинженер в ракетно-космической промышленности занимается
                              разработкой, модернизацией, изготовлением и сопровождением
                              радиотехнических систем и радиоэлектронных средств, решающих задачи
                              навигации, телеметриии, радиоизмерений, телевидения, регистрации
                              воздействий, дистанционного управления, радиолокации и связи. В его
                              обязанности входит также разработка конструкторской и эксплуатационной
                              документации по созданию радиоэлектронных средств, разработка,
                              сопровождение и отладка программного обеспечения.
                              <br />
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
               <div
                  style={{
                     paddingTop: "30px"
                  }}
                  className="projects-clean"
               >
                  <div className="container">
                     <div className="intro">
                        <h2 className="text-center">Проекты</h2>
                     </div>
                     <div className="row projects">
                        <div className="col-sm-6 col-lg-4 item">
                           <img className="img-fluid" src="./assets/img/1.jpg" />
                           <h3 className="name">
                              Антенная система для аэрологической станции <br />
                           </h3>
                           <p className="description">
                              Разработка антенной системы для направленного приема сигнала
                              радиозондов, находящихся в атмосфере, и передающих данные с датчиков
                              скорости, влажности и т.д. на наземную станцию.
                              <br />
                           </p>
                        </div>
                        <div className="col-sm-6 col-lg-4 item">
                           <img className="img-fluid" src="./assets/img/2.jpg" />
                           <h3 className="name">
                              Миниатюрная радиолокационная станция
                              <br />
                           </h3>
                           <p className="description">
                              Разработка радиолокационной станции на поворотной платформе,
                              предназначенной для слежения за объектом внутри помещения.&nbsp;
                              <br />
                           </p>
                        </div>
                        <div className="col-sm-6 col-lg-4 item">
                           <img className="img-fluid" src="./assets/img/3.png" />
                           <h3 className="name">
                              Антенная система для медицинского радиометра
                              <br />
                           </h3>
                           <p className="description">
                              Антенная система, предназначенной для неинвазивных измерений
                              собственного электромагнитного излучения головного мозга.&nbsp;
                              <br />
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
               <div className="team-clean">
                  <div className="container">
                     <div className="intro">
                        <h2 className="text-center">Выпускники</h2>
                     </div>
                     <div className="row people">
                        <div className="col-md-6 col-lg-4 item">
                           <img
                              className="rounded-circle"
                              src="./assets/img/cherev.jpg"
                              width={150}
                              height={150}
                              style={{
                                 marginLeft: "40px"
                              }}
                           />
                           <h3 className="name">Валерий Черев</h3>
                           <p className="title">Выпускник Направления</p>
                           <p className="description" style={{ fontSize: 18 }}>
                              После получения диплома мои взгляды на жизнь не изменились.
                              Университет дал мне базовые знания по специальности, умение находить
                              нужную информацию и находить выход из разных ситуаций. Большой интерес
                              у меня вызывали дисциплины связанные с разработкой реальных устройств.
                              В УрФУ все преподаватели высококвалифицированные, но не каждый умеет
                              заинтересовать в своем предмете. Учиться не легко, но справиться
                              можно. Первокурсникам я советую учиться, учиться и ещё раз учиться,
                              чтобы стать хорошим специалистом, потому что после получения диплома
                              трудоустроиться легко, всё будет зависеть от желаний.
                           </p>
                           {/* <div className="social">
                           <a>
                              <i className="fa fa-facebook-official" />
                           </a>
                           <a>
                              <i className="fa fa-twitter" />
                           </a>
                           <a>
                              <i className="fa fa-instagram" />
                           </a>
                        </div> */}
                        </div>
                        <div className="col-md-6 col-lg-4 item">
                           <img
                              className="rounded-circle"
                              style={{
                                 marginLeft: "40px"
                              }}
                              src="./assets/img/fahr.jpg"
                              width={150}
                              height={150}
                           />
                           <h3 className="name">Вадим Фахритидинов</h3>
                           <p className="title">Выпускник направления</p>
                           <p className="description" style={{ fontSize: 18 }}>
                              После получения диплома ко мне пришло осознания того, что началась
                              по-настоящему взрослая жизнь. В университете мне дали базовые знания
                              по специальности. Большой интерес у меня вызывали предметы связанные с
                              проектированием различных СВЧ устройств, так же в системе
                              автоматизированного проектирования (САПР) меня очень заинтересовали.
                              Преподаватели в УрФУ достаточно квалифицированные, но не все умеют
                              заинтересовать предметом и это проблема. Обучение по направлению
                              достаточно сложное, но если делать и сдавать вовремя, то учиться
                              становится достаточно просто. Устроится на работу легко, но, конечно,
                              чтобы попасть на хорошее, оплачиваемое место, нужно обладать высоким
                              уровнем знаний своей специальности. Первокурсникам советую
                              дополнительно заниматься дома, изучать английский язык и пару языков
                              программирования, в дальнейшем это очень пригодится.
                           </p>
                           {/* <div className="social">
                           <a>
                              <i className="fa fa-facebook-official" />
                           </a>
                           <a>
                              <i className="fa fa-twitter" />
                           </a>
                           <a>
                              <i className="fa fa-instagram" />
                           </a>
                        </div> */}
                        </div>
                        <div className="col-md-6 col-lg-4 item">
                           <img
                              className="rounded-circle"
                              src="./assets/img/triph.jpg"
                              height={150}
                              style={{
                                 marginLeft: "40px",
                                 maxWidth: "160px"
                              }}
                           />
                           <h3 className="name">Данил Трифонов</h3>
                           <p className="title">Выпускник направления</p>
                           <p className="description" style={{ fontSize: 18 }}>
                              После получения диплома произошло “обнуление счётчика”, переход в
                              другую стадию жизни, более ответственную и самостоятельную. По моему
                              мнению, университет дает многое - нужно лишь этим воспользоваться. Для
                              меня многие предметы были интересны из-за личных качеств
                              преподавателей. Учиться на направлению радиотехника не сложно, главное
                              не лениться и понимать, что твои результаты зависят от твоих усилий.
                              Первокурсникам я бы пожелал терпения и хороших одногруппников. По
                              опыту моих знакомых трудоустроиться после получения диплома не сложно
                              <br />
                           </p>
                           {/* <div className="social">
                           <a>
                              <i className="fa fa-facebook-official" />
                           </a>
                           <a>
                              <i className="fa fa-twitter" />
                           </a>
                           <a>
                              <i className="fa fa-instagram" />
                           </a>
                        </div> */}
                        </div>
                     </div>
                  </div>
               </div>
               <div className="article-clean">
                  <div className="container">
                     <div className="row">
                        <div className="col-lg-10 col-xl-8 offset-lg-1 offset-xl-2">
                           <div className="intro">
                              <h1 className="text-center">
                                 Новая веха в развитии института
                                 <br />
                              </h1>
                              <img className="img-fluid" src="./assets/img/rtf2016.jpg" />
                           </div>
                           <div className="text">
                            
                          </div>
                           <img className="img-fluid" src="./assets/img/roof.jpg" />
                        </div>
                     </div>
                  </div>
               </div>
               <div
                  className="container responsive"
                  style={{
                     marginTop: 25,
                     maxWidth: 760
                  }}
               >
                  <div className="card-deck mb-3 text-center">
                     <div className="card mb-4 shadow-sm">
                        <div className="card-header">
                           <h4 className="my-0 font-weight-normal">
                              Средняя зарплата в сфере <br /> <br />
                           </h4>
                        </div>
                        <div className="card-body">
                           <h1 className="card-title pricing-card-title">
                              $500 <small className="text-muted">/ месяц</small>
                           </h1>
                           <ul className="list-unstyled mt-3 mb-4">
                              {/*               <li>20 users included</li>
            <li>10 GB of storage</li>
            <li>Priority email support</li>
            <li>Help center access</li> */}
                           </ul>
                        </div>
                     </div>
                     <div className="card mb-4 shadow-sm">
                        <div className="card-header">
                           <h4 className="my-0 font-weight-normal">
                              Средняя зарплата выпускников направления
                           </h4>
                        </div>
                        <div className="card-body">
                           <h1 className="card-title pricing-card-title">
                              $570 <small className="text-muted">/ месяц</small>
                           </h1>
                           <ul className="list-unstyled mt-3 mb-4">
                              {/*               <li>30 users included</li>
            <li>15 GB of storage</li>
            <li>Phone and email support</li>
            <li>Help center access</li> */}
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               <div className="features-clean">
                  <div className="container">
                     <div className="intro">
                        <blockquote className="blockquote text-center">
                           <p className="mb-0">
                              Образование придает человеку достоинство, да и раб начинает сознавать,
                              что он не рожден для рабства.
                              <br />
                           </p>
                           <footer className="blockquote-footer">
                              <a href="https://ru.wikipedia.org/wiki/%D0%94%D0%B8%D0%B4%D1%80%D0%BE,_%D0%94%D0%B5%D0%BD%D0%B8">
                                 <em>Дидро Д.</em>
                              </a>
                              <br />
                           </footer>
                        </blockquote>
                     </div>
                  </div>
               </div>
               <div className="footer-basic">
                  <footer>
                     <div className="social">
                        {/* <a>
                        <i
                           className="fa fa-telegram"
                           style={{
                              margin: 0,
                              padding: 0,
                              marginTop: 0,
                              marginBottom: 0,
                              marginRight: 0,
                              marginLeft: 0
                           }}
                        />
                     </a> */}
                        <a href="https://vk.com/iritrtf_rt">
                           <i className="fa fa-vk" />
                        </a>
                     </div>
                     <ul className="list-inline">
                        <li className="list-inline-item" />
                        <li className="list-inline-item">
                           <a />
                        </li>
                        <li className="list-inline-item">
                           <a href="https://programs.edu.urfu.ru/ru/8674/">
                              Обр. программа на сайте УрФУ
                           </a>
                        </li>
                        <li className="list-inline-item">
                           <a href="https://vk.com/ymitelman">Руководитель программы</a>
                        </li>
                        <li className="list-inline-item">
                           <a href="https://programs.edu.urfu.ru/ru/8674/documents/">Документы</a>
                        </li>
                     </ul>
                     <p className="copyright">Cyberdata. © 2018</p>
                  </footer>
               </div>
            </div>
         </div>
      );
   }
}
