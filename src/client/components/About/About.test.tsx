import { configure, mount, shallow } from "enzyme";
import * as Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });
import { should } from "fuse-test-runner";
import "jsdom-global/register";
import * as React from "react";
import About from "./About";

export class AboutTest {
   public "Should be okay"() {
      const wrapper = mount(<About />);
      should(wrapper).beOkay();
      // wrapper.find(".button").simulate("click");
      // const countText = wrapper.find(".count").text();
      // should(countText)
      //    .beString()
      //    .equal("1");
   }
}
