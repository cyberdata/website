import "bootstrap/dist/css/bootstrap.min.css";
import * as React from "react";
import { hydrate } from "react-dom";
import { BrowserRouter } from "react-router-dom";
import App from "./components/App/App";
const wrapper = (
   <BrowserRouter>
      <App />
   </BrowserRouter>
);
hydrate(wrapper, document.getElementById("root"));
