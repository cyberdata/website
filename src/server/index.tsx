import cookieParser = require("cookie-parser");
import * as express from "express";
import path = require("path");
import * as React from "react";
import { renderToString } from "react-dom/server";
import { StaticRouter } from "react-router";
import App from "../client/components/App/App";
const server = express();
const port = 80;

server.use(express.json());
server.use(express.urlencoded({ extended: false }));
server.use("/static", express.static(path.resolve("./dist/client")));
server.use("/assets", express.static("C:\\Users\\User\\Desktop\\website\\src\\public"));

server.use(cookieParser());
server.get("/", (req, res) => {
   const context = {} as any;

   const wrapper = (
      <StaticRouter location={req.url} context={context}>
         <App />
      </StaticRouter>
   );
   if (context.url) return res.redirect(context.url);
   const answer = `
      <!doctype html>
      <html>
          <head>
               <meta name="viewport" content="width=device-width, initial-scale=1.0">
              <title>Радиотехника в УрФУ</title>
              <link rel="stylesheet" href="assets/fonts/font-awesome.min.css" />
              <link rel="stylesheet" href="assets/fonts/ionicons.min.css" />
              <link rel="stylesheet" href="assets/fonts/material-icons.min.css" />
              <link rel="stylesheet" href="assets/fonts/simple-line-icons.min.css" />
              <link rel="stylesheet" href="assets/fonts/typicons.min.css" />
              <link
                 rel="stylesheet"
                 href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700"
              />
              <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
                    <link rel="stylesheet" href="assets/css/Features-Clean.css">
                 <link rel="stylesheet" href="assets/css/Footer-Basic.css">
                 <link rel="stylesheet" href="assets/css/Header-Blue.css">
                 <link rel="stylesheet" href="assets/css/Highlight-Blue.css">
                 <link rel="stylesheet" href="assets/css/Highlight-Phone.css">
                 <link rel="stylesheet" href="assets/css/Navigation-Clean.css">
                 <link rel="stylesheet" href="assets/css/Article-Clean.css">
                 <link rel="stylesheet" href="assets/css/priem_rtf.css">
                 <link rel="stylesheet" href="assets/css/styles.css">
        

          </head>
          <body>
              <div id='root'>${renderToString(wrapper)}</div>
              <script src='./static/bundle.js'></script>
          </body>
      </html>
   `;
   res.send(answer);
});
const Server = server.listen(port, () => {
   console.log(`http://localhost:${port}`);
});

/**
 * Used to restart server by fuseBox
 */
export async function shutdown() {
   Server.close();
}
